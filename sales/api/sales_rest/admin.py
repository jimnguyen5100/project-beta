from django.contrib import admin
from .models import SalesPerson, Customer, Sales, AutomobileVO

# Register your models here.
admin.site.register(SalesPerson)
admin.site.register(Customer)
admin.site.register(Sales)
admin.site.register(AutomobileVO)

class SalesPersonAdmin(admin.ModelAdmin):
  pass
class CustomerAdmin(admin.ModelAdmin):
  pass
class SalesAdmin(admin.ModelAdmin):
  pass
class AutomobileVO(admin.ModelAdmin):
  pass