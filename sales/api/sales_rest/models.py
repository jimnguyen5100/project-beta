from enum import unique
from django.db import models
from django.urls import reverse
# from phone_field import PhoneField

# Create your models here.

class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=100, null=True)
    # model = models.CharField(max_length=100)
    # manufacturer = models.CharField(max_length=100)


    # def get_api_url(self):
    #     return reverse("api_automobile", kwargs={"import_href": self.import_href})
    def __str__(self):
        return self.import_href

class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"name": self.name})

    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=10)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"name": self.name})

    def __str__(self):
        return self.name

class Sales(models.Model):
    automobile = models.ForeignKey(AutomobileVO, related_name="sales", on_delete=models.PROTECT)
    sales_person = models.ForeignKey(SalesPerson, related_name="sales", on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, related_name="sales", on_delete=models.PROTECT)
    sale_price = models.CharField(max_length=50)

    def get_api_url(self):
        return reverse("api_sales", kwargs={"pk": self.pk})

    # def __str__(self):
    #   return self.sales_person.name

    def __str__(self):
      return f"{self.automobile}"
