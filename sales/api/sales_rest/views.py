from pickle import FALSE
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder


from .encoders import (
    # AutomobileVoEncoder,
    SalesPersonEncoder,
    CustomerEncoder,
    SalesEncoder
)
from .models import AutomobileVO, SalesPerson, Customer, Sales

class AutomobileVoEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
      "id",
      "import_vin",
      "year",
      "color",
      "import_href"
      # "model",
      # "manufacturer"
    ]


#SalesPerson:-----------------------------------
@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    #List all sales persons
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder,
            safe=False
        )
    #Create new sales person
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False
        )

@require_http_methods(["DELETE","GET", "PUT"])
def api_sales_person(request,pk):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=pk)

        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: #Put
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=pk)
            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response =  JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

#Customer:-----------------------------------------------
@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder=CustomerEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
          customer,
          encoder=CustomerEncoder,
          safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)
            props = ["name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
              response = JsonResponse({"message": "Does not exist"})
              response.status_code = 404
              return response

#Sales:-----------------------------------------------
@require_http_methods(["GET", "POST"])
def api_sales(request): #list
    if request.method == "GET":
        sales = Sales.objects.all()

        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
            # safe=False
        )
    else:
        content = json.loads(request.body)
        # try:

        automobile = AutomobileVO.objects.get(import_href=content["automobile"])

        content["automobile"] = automobile

        sales_person = SalesPerson.objects.get(employee_number=content["sales_person"])


        content["sales_person"] = sales_person

        customer = Customer.objects.get(id=content["customer"])


        content["customer"] = customer

        sale = Sales.objects.create(**content)
        return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        # except:
          # passxw
            # response = JsonResponse(
            #     {"message": "Could not create sales record"}
            # )
            # response.status_code = 400
            # return response

@require_http_methods(["DELTE", "GET", "PUT"])
def api_sale(request, pk): #detail
    if request.method == "GET":
        try:
            sale = Sales.objects.get(id=pk)
            return JsonResponse(
              sale,
              encoder=SalesEncoder,
              safe=False
            )
        except Sales.DoesNotExist:
            response = JsonResponse({"message": "Record does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = Sales.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
              sale,
              encoder=SalesEncoder,
              safe=False
            )
        except Sales.DoesNotExist:
            return JsonResponse({"message": "Sales record not exist"})
    else:
        try:
            content = json.loads(request.body)
            sale = Sales.objects.get(id=pk)

            props = ["automobile", "sales_person", "customer", "sale_price"]
            for prop in props:
                setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False
            )
        except Sales.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET"])
def automobile_vo(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVoEncoder,
            safe=False,
        )
