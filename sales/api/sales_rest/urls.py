from django.urls import path
from .views import api_sales_persons, api_sales_person, api_customers, api_customer, api_sales,  api_sale, automobile_vo

urlpatterns = [
    path("salesperson/", api_sales_persons, name="api_sales_persons"),
    path("salesperson/<int:pk>/", api_sales_person, name="api_sales_person"),
    path("customer/", api_customers, name="api_customers"),
    path("customer/<int:pk>/", api_customer, name="api_customer"),
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:pk>/", api_sale, name="api_sale"),
    path('automobileVO/', automobile_vo, name="automobile_vo_list"),
]