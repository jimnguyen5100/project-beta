import React from 'react';

class CustomerForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      address: '',
      phone_number: '',
    }
    this.handleNameChange = this.handleNameChange.bind(this)
    this.handleAddressChange = this.handleAddressChange.bind(this)
    this.handelePhoneChange = this.handelePhoneChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleNameChange(event){
    const value = event.target.value
    this.setState({ name: value})
  }
  handleAddressChange(event){
    const value = event.target.value
    this.setState({ address: value})
  }
  handelePhoneChange(event){
    const value = event.target.value
    this.setState({ phone_number : value})
  }
  async handleSubmit(event){
    event.preventDefault()
    const data = { ...this.state }

    const url = "http://localhost:8090/api/customer/"
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
        const newCustomer = await response.json()
    }
    const cleared = {
        name: '',
        address: '',
        phone_number: ''
    }
    this.setState(cleared)
  }


  render() {
    return (
      <>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="text-center" >Add a customer</h1>
            <form onSubmit={this.handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name:</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.phone_number} onChange={this.handelePhoneChange} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                <label htmlFor="phone_number">Phone Number:</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.address} onChange={this.handleAddressChange} placeholder="Adress" required type="text" name="address" id="address" className="form-control" />
                <label htmlFor="address">Address:</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
  }
}
export default CustomerForm