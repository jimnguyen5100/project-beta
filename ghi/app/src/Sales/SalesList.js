// import App from './App'
import React from 'react';

class SalesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sales: []
    }
  }
  async componentDidMount() {
    const url = "http://localhost:8090/api/sales/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      this.setState({ sales: data.sales })
    }
  }
  render() {
    return (
      <>
        <br></br>
        <br></br>
        <br></br>
        <h1 className="dislplay-1 text-center" >Sales Records</h1>
        <br></br>
        <br></br>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.sales?.map((sale, i) => {
              return (
                // console.log("sales::", sale)
                <tr key={i}>
                  <td>{sale.sales_person.name}</td>
                  <td>{sale.customer.name}</td>
                  <td>{sale.automobile.import_vin}</td>
                  <td>{sale.sale_price}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </>
    )
  }
}

export default SalesList;