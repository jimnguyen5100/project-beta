import React from 'react';

class AutoForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      color: '',
      year: '',
      vin: '',
      model_id: '',
      models: []
    }
    this.handleColorChange = this.handleColorChange.bind(this)
    this.handleYearChange = this.handleYearChange.bind(this)
    this.handleVinChange = this.handleVinChange.bind(this)
    this.handleModelChange = this.handleModelChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleColorChange(event) {
    const value = event.target.value
    this.setState({ color: value })
  }
  handleYearChange(event) {
    const value = event.target.value
    this.setState({ year: value })
  }
  handleVinChange(event) {
    const value = event.target.value
    this.setState({ vin: value })
  }
  handleModelChange(event) {
    const value = event.target.value
    this.setState({ model_id: value })
  }

  async handleSubmit(event) {
    event.preventDefault()
    const data = { ...this.state }
    delete data.models
    const url = "http://localhost:8100/api/automobiles/"
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      const newAuto = await response.json()
    }
    const cleared = {
      color: '',
      year: '',
      vin: '',
      models: [],
      model_id: ''
    }
    this.setState(cleared)
  }
  async componentDidMount() {
    const models_url = 'http://localhost:8100/api/models/'
    const models_response = await fetch(models_url)
    if (models_response.ok) {
      const data = await models_response.json();
      this.setState({ models: data.models })
    }
  }


  render() {
    return (
      <>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1 className="text-center" >Add a automobile</h1>
              <form onSubmit={this.handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                  <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color:</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={this.state.year} onChange={this.handleYearChange} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                  <label htmlFor="year">Year:</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={this.state.vin} onChange={this.handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                  <label htmlFor="vin">Vin:</label>
                </div>
                <div>
                  <select onChange={this.handleModelChange} value={this.state.model_id} name="model_id" required id="model_id" className="form-select">
                    <option value="">Choose a model</option>
                    {this.state.models?.map(model => {
                      return (
                        <option key={model.id} value={model.id}>
                          {model.name}
                        </option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default AutoForm