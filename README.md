
![](Images/Logo.png)

## Application Manages
![](Images/servicesicon.png)


## Team:
* Service Dev: Jimmy Nguyen
* Sales Dev: Thinh Mai

## Setup
![](Images/Setup.png)

## Design
![](Images/AppDiagram.png)

## Service microservice
![](Images/ServiceDiagram.png)

## Sales microservice
![](Images/SalesDiagram.png)

## Insomnia for service
Statuses need to be made first in admin
+ Please go in docker service-api-1 terminal
+ In docker terminal create super user with: python manage.py createsureuser
+ In browser, type: http://localhost:8080/admin/
+ Add new statuses:
  + Id: 1, Name: Scheduled
  + Id: 2, Name: Cancel
  + Id: 3, Name: Finish


<details>
<summary>Technician</summary>
<br>

List all technicians: GET: http://localhost:8080/api/technician/

Create new technician: POST: http://localhost:8080/api/technician/

+ Layout for JSON body: {"name": "John", "employee_number: "3"}

Update a technician: PUT: http://localhost:8080/api/technician/ID/

+ Layout for JSON body: {"name": 'Bob', "employee_number: "2"}

Delete a sales person: DELETE: http://localhost:8080/api/technician/ID/
</details>

<details>
<summary>Service</summary>
<br>

List all service: GET: http://localhost:8080/api/services/

List a service by vin http://localhost:8080/api/services/5678nissan/

Create a service request: POST: http://localhost:8080/api/services/

+ Layout for JSON body: {"vin": 5678nissan, "customer": "Jim, "reason: "oil change", "technician": 1}

Update a service: PUT: http://localhost:8080/api/services/5678nissan/

+ Layout for JSON body: {"vin": 5678nissan, "customer": "Bob, "reason: "flat tire", "technician": 1}

Delete a service: DELETE: http://localhost:8080/api/services/5678nissan/
</details>

## Insomnia for sales

<details>
<summary>Sales Person</summary>
<br>

List all sales people: GET: http://localhost:8090/api/salesperson/

Create new sales person: POST: http://localhost:8090/api/salesperson/

+ Layout for JSON body: {"name": "salesperson3","employee_number": "3"}

Update a sales person: PUT: http://localhost:8090/api/salesperson/1/

+ Layout for JSON body: {"name": "Bob","employee_number": 2}

Delete a sales person: DELETE: http://localhost:8090/api/salesperson/1/
</details>

<details>
<summary>Customer</summary>
<br>

List all customers: GET: http://localhost:8090/api/customer/

Create new customer: POST: http://localhost:8090/api/customer/

+ Layout for JSON body: {"name": "customter3","phone_number": "1234567890","address": "1234 Address St"}

Update customer: PUT: http://localhost:8090/api/customer/1/

+ Layout for JSON body: {"name": "John","phone_number": "0987654321","address": "1235 Address St"}

Delete a customer: DELETE: http://localhost:8090/api/customer/1/
</details>

<details>
<summary>Sales</summary>
<br>

List all sales: GET: http://localhost:8090/api/sales/

Create a sale: POST: http://localhost:8090/api/sales/

+  Layout for JSON body: {"sales_person": 3,"customer": 1,"sale_price": 20000,"automobile": "Import Href"}

Update a sale: PUT: http://localhost:8090/api/sales/1/

+ Layout for JSON body: {"sales_person": 3,"customer": 1,"sale_price": PRICE,"automobile": Import Href}

Delete a sale: DELETE: http://localhost:8090/api/sales/1/
</details>

## Insomnia for inventory

<details>
<summary>Manufacturer</summary>
<br>

List all manufacturer: GET: http://localhost:8100/api/manufacturers/

Create a manufacturer: POST: http://localhost:8100/api/manufacturers/

+ Layout for JSON body: {"name": "Nissan"}

Update a manufacturer: PUT: http://localhost:8100/api/manufacturers/1/

+ Layout for JSON body: {"name": 'Honda'}

Delete a manufacturer: DELETE: http://localhost:8100/api/manufacturers/1/
</details>

<details>
<summary>Vehicle Model</summary>
<br>

List all vehicle models: GET: http://localhost:8100/api/models/

Create new vehicle model: http://localhost:8100/api/models/

+ Layout for JSON body: {"name": "Gt-R","picture_url": Image,"manufacturer_id": 1}

Update a model: PUT: http://localhost:8100/api/models/1/

+ Layout for JSON body: {"name": "Gt-R","picture_url": Image,"manufacturer_id": ID}

Delete a model: DELETE: http://localhost:8100/api/models/1/
</details>

<details>
<summary>Automobile</summary>
<br>

List all automobiles: GET: http://localhost:8100/api/automobiles/

Create an automobile: POST: http://localhost:8100/api/automobiles/

+ Layout for JSON body: {"color": "white","year": 2022,"vin": "5678nissan","model_id": 1}

Update an automobile: PUT: http://localhost:8100/api/automobiles/VIN/

+  Layout for JSON body: {"color": "white","year": 2022,"vin": "5678nissan","model_id": ID}

Delete an automobile: DELETE: http://localhost:8100/api/automobiles/VIN/
</details>

## Additional Info

<details>
<summary>Urls and Ports for Microservices</summary>
<br>

Sales: http://localhost:8090/

+ port: 8090

Service: http://localhost:8080/

+ port: 8080

Inventory: http://localhost:8100/

+ port: 8100

React Website: http://http://localhost:3000/

+ port: 3000
<br>

</details>
